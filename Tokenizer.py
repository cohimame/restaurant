# -*- coding: utf-8 -*-
import StopWords
import PoemHTMLParser
import Normalizer 
import Splitter

stopwords = StopWords.collect_stopwords()

def tokenize(poem_file):
  result = []
  poems  = PoemHTMLParser.extract_poems(poem_file)
  for poem in poems:
    npoem   = Normalizer.normalize(poem)
    words   = Splitter.split(npoem)
    nonstop = [w for w in words if w not in stopwords]
    if [] != nonstop:
      p = u' '.join(nonstop)+u'\n'
      result.append(p)
  return result    
