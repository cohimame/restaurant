# -*- coding: utf-8 -*-
import re
import codecs

ru = re.compile(u"([а-яё]+([-–][а-яё]+)+|[а-яё][а-яё]+)")

def split(text):
  return [token[0] for token in ru.findall(text)]