# -*- coding: utf-8 -*-
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

import Data
import Lemmatizer
import StopWords
import Io

class TfIdfMatrix:
  def __init__(self, documents, labels):
    self.vectorizer = TfidfVectorizer(tokenizer=Lemmatizer.lemmatize,
                                      stop_words = StopWords.collect_stopwords())
    self.tfidfMatrix = self.vectorizer.fit_transform(documents)
    self.labels = labels

  def transform(self,query):
    return self.vectorizer.transform([query])

  def nearest_documents_indices(self, query, k):
    vec      = self.transform(query)
    sim_list = cosine_similarity(vec, self.tfidfMatrix)[0]
    return sorted(range(len(sim_list)), key=lambda i: sim_list[i], reverse=True)[:k]

  def nearest_document_labels(self, query, k):
    topi = self.nearest_documents_indices(query, k)
    topl = [self.labels[i] for i in topi ]
    return topl


if __name__ == "__main__":

  dataset = Data.Menu()

  X_train, y_train = dataset.data[0:200], dataset.labels[0:200]
  X_test,  y_test  = dataset.data[-100:],  dataset.labels[-100:]

  matrix = TfIdfMatrix(X_train,y_train)

  for idx in range(0,10):
    top4labs =  matrix.nearest_document_labels(X_test[idx],8)
    print "input vector has label: " + Io.list_to_string(y_test[idx])
    print "top labels for this vector are:"
    for l in top4labs:
      print Io.list_to_string(l)
    print ""
