# -*- coding: utf-8 -*-
import codecs
from collections import Counter

def count_word_freq(words):
  return Counter(words).most_common()

def save_stats(output, stats):
  output = codecs.open(output, mode='w',encoding='utf-8')
  for wf in stats:
    w = wf[0]
    f = str(wf[1])
    output.write(u'{0} - {1}\n'.format(w, f))
  output.close()