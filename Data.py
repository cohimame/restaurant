# -*- coding: utf-8 -*-
import codecs
import Statistics
import Splitter

DATA_PATH = "Data/menu.txt"

class Menu:

  def __read_menu(self, source):
    src = codecs.open(source, encoding='utf-8')
    data = []
    labels = []
    for line in src:
      tokens = line.split(None, 2)
      if len(tokens)==3:
        labels += [tokens[1].split("/")]
        data   += [tokens[2]]
    src.close()

    return labels, data


  def __init__(self):
    self.labels, self.data = self.__read_menu(DATA_PATH)


  def __repr__(self):
    length = len(self.labels)
    return u"""labeled dataset, has {0} multi-labeled documents""".format(length)


  def label_statistics(self):
    labels = [item for sublist in self.labels for item in sublist]
    return Statistics.count_word_freq(labels)

  def data_statistics(self):
    tokens = []
    for text in self.data:
        tokens += Splitter.split(text.lower())
    return Statistics.count_word_freq(tokens)


if __name__ == "__main__":
  menu = Menu()
  print menu
  Statistics.save_stats("Statistics/label_freqs",menu.statistics())
  Statistics.save_stats("Statistics/word_freqs",menu.data_statistics())
